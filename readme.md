## Instructions
Installer l'environnement nécessaire au fonctionnement de l'api
Compléter l'ExoController et la route "fixme" afin de remonter le nombre d’antennes actives 
et inactives qui ne sont pas deleted. On regroupera ce nombre par nom de fabriquant.
Les données se trouvent dans le fichier antennes.db à la racine du projet

### Points attendus :
* Retour au format JSON :
  [
     {"fabriquant":"HydroSol","actif":0,"nb_antennes":30},
     {"fabriquant":"OxygeneGt","actif":0,"nb_antennes":12},
     {"fabriquant":"SolGok","actif":1,"nb_antennes":42}
  ]
* Utilisation de la classe TestService afin de récupérer les données


