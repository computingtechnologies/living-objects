<?php


namespace App\Controller;

use App\Service\TestService;
use Doctrine\DBAL\Driver\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ExoController extends AbstractController
{
    /**
     * @Route("/index",name="index")
     */
    public function index()
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/fixme", name="fixme")
     */
    public function fixme(TestService $service)
    {
        /**
         * TODO:  use entities, repositories (are we allowed to modify in the database?)
         *  query speed can be increased if we put a primary or index in the db
         *  Expose URIs
         *  Implement a REST library
         *  Add authentication
         *  The above might not be required because we only have 20mins
         */
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $antennasStats = false;
        try {
            $antennasStats = $service->getAntennaStats();
            $response->setContent(json_encode($antennasStats));
            $response->setStatusCode(200);
        } catch (Exception $e) {
            $response->setStatusCode(500);
            $response->setContent($e->getMessage());
        } catch (\Doctrine\DBAL\Exception $e) {
            $response->setStatusCode(500);
            $response->setContent($e->getMessage());
        }
        if (!$antennasStats) {
            $response->setStatusCode(404);
        }
        return $response;
    }
}
