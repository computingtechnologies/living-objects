<?php


namespace App\Service;


use Doctrine\ORM\EntityManagerInterface;

class TestService
{
    /**
     * @var EntityManagerInterface $em
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @return false|array
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function getAntennaStats()
    {
        $sql = 'SELECT fabriquant,CAST(actif as int) as actif,COUNT(*) as nb_antennes FROM antenne WHERE deleted=0 GROUP BY fabriquant,actif
ORDER BY actif';

        $stmt = $this->em->getConnection()->prepare($sql);
        $query = $stmt->executeQuery();
        if ($results = $query->fetchAllAssociative()) {
            return array_map(function($result){//TODO: this can be easily mapped in ORM
                $result['nb_antennes'] = intval($result['nb_antennes']);
                $result['actif'] = intval($result['actif']);
                return $result;
            }, $results);
        }
        return false;
    }
}
